//
//  ViewController.swift
//  google
//
//  Created by korvincorey on 6/9/18.
//  Copyright © 2018 korvincorey. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleAPIClientForREST

class ViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
   
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheetsReadonly]
    private let service = GTLRSheetsService()
    let todayDate = Date()
    var functionCalls = Int()
    var usersName = String()
    var userIndex = Int()
    var arrayCounter = Int()
    var dishesArray = [String]()
    var usersChoice = [String]()
    var arrayCounterForChoice = Int()
    var godDamnedUserChoice = "Today, you will receive gods blessing in the form of: \n"
    
    @IBOutlet weak var listView: UITextView!
    @IBOutlet weak var GoogleSI: GIDSignInButton!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var usersNameOutlet: UITextField!
    
    
    
    @IBAction func signOutButton(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut()
        GoogleSI.isHidden = false
    }
    
    @IBAction func menuButton(_ sender: Any) {
        let dateForLunch = dateForSheet()
        functionCalls = 4
        if dateForLunch == "вторник" {              //add special check beacuse all week days have space after it's name, except Tuesday
            listOfDishes(range: "\(dateForLunch)!B\(userIndex):\(userIndex)")    // order row
        } else {
            listOfDishes(range: "\(dateForLunch) !B\(userIndex):\(userIndex)")
            print("posle metoda 3 \(userIndex)")
        }
    }
    
    func dateForSheet() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let strForDate = dateFormatter.string(from: todayDate as Date)
        return strForDate
    }
    
    func listOfDishes(range: String) {
        listView.text = "Imagine miracle here :O"
        let spreadsheetId = "1NrPDjp80_7venKB0OsIqZLrq47jbx9c-lrWILYJPS88"
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet
            .query(withSpreadsheetId: spreadsheetId, range:range)
        service.executeQuery(query,
                             delegate: self,
                             didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:))
        )
    }
    
    // Process the response and display output
    @objc func displayResultWithTicket(ticket: GTLRServiceTicket,
                                 finishedWithObject result : GTLRSheets_ValueRange,
                                 error : NSError?) {
        
        if let error = error {
            listView.text = "Whoops (:"
            showAlert(title: "Error", message: error.localizedDescription)
            return
        }
        
        functionCalls += 1
        print("\(functionCalls) vizov posle incrementa")
        
        let cellsArray = result.values!
        
        if cellsArray.isEmpty {
            listView.text = "No data found."
            return
        }
        
        if functionCalls == 1  {
            for cell in cellsArray {
                arrayCounter += 1
                var stringModifier = ""
                stringModifier += "\(cell)"
                stringModifier.removeFirst()
                stringModifier.removeLast()
                if stringModifier == usersName {
                    userIndex = arrayCounter + 2  //because of user list started from third row
                    print("we got match! userIndex in Array is \(userIndex) and user is \(stringModifier)")
                }
            }
            print("1st circle with users, UI is - \(userIndex)     \(functionCalls)")
        } else if functionCalls == 2 {
            var stringModifier = ""
            stringModifier += "\(cellsArray[0])"
            dishesArray = stringModifier.components(separatedBy: ", ")
            dishesArray[0].removeFirst()
            print(dishesArray[0])
//            dishesArray[11].removeLast()
//            print(dishesArray[11])
            print("2nd circle with dishes, UI is - \(userIndex)     \(functionCalls)")
        } else if functionCalls == 5  {
            print("3rd circle with user selections, UI is - \(userIndex)      \(functionCalls)")
            print(" tut 4toto ne tak \(cellsArray)")
            var stringModifier = ""
            stringModifier += "\(cellsArray[0])"
            stringModifier.removeLast()
            stringModifier.removeFirst()
            usersChoice = stringModifier.components(separatedBy:", ")

            print("usersChoice is \(usersChoice)")
            print("dishesArray is \(dishesArray)")
            for choice in usersChoice {
                arrayCounterForChoice += 1
                if choice.isEmpty == false {
                    godDamnedUserChoice += "\(dishesArray[arrayCounterForChoice - 1]) \n"
                }
            }
            listView.text = godDamnedUserChoice
            
            userIndex = 0
            functionCalls = 0
            arrayCounter = 0
            dishesArray = []
        }
        //listView.text = "userindex \(userIndex) + functionCalls \(functionCalls)     \(functionCalls)"
    }
    
    
    // Helper for showing an alert
    func showAlert(title : String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil
        )
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            showAlert(title: "Authentication Error", message: error.localizedDescription)
            self.service.authorizer = nil
        } else {
            self.GoogleSI.isHidden = true
            self.listView.isHidden = false
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            //usersName = "Дэвид Робинсон"
            usersName = usersNameOutlet.text!

            let dateForLunch = dateForSheet()
            if dateForLunch == "вторник" {              //add special check beacuse all week days have space after it's name, except Tuesday
                listOfDishes(range: "\(dateForLunch)!A3:A34")   // list of users
                listOfDishes(range: "\(dateForLunch)!B2:M2")    // list of dishes
            } else {
                listOfDishes(range: "\(dateForLunch) !A3:A34")
                print("posle metoda 1 \(userIndex)")
                listOfDishes(range: "\(dateForLunch) !B2:M2")
                print("posle metoda 2 \(userIndex)")
            }
            // Permanent Data Storage
            UserDefaults.standard.set(usersNameOutlet.text, forKey: "name")
            
            let nameObject = UserDefaults.standard.object(forKey: "name")
            
            if let name = nameObject as? String {
                
                usersName = name
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

